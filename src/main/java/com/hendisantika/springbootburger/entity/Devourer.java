package com.hendisantika.springbootburger.entity;

import lombok.*;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-burger
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/09/18
 * Time: 08.11
 * To change this template use File | Settings | File Templates.
 */

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Devourer {
    private String devourerName;
    private int burgerId;
}
