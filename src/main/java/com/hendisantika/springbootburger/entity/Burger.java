package com.hendisantika.springbootburger.entity;

import lombok.*;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-burger
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/09/18
 * Time: 08.10
 * To change this template use File | Settings | File Templates.
 */

@Data
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Burger {
    private String burgerName;
    private boolean devoured;

    // Getter for Burger Status
    public boolean getDevoured() {
        return devoured;
    }
}
