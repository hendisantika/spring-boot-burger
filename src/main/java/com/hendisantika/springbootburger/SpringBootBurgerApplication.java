package com.hendisantika.springbootburger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBurgerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootBurgerApplication.class, args);
    }
}
